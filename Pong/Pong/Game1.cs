﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace Pong
{
    /// <summary>
    /// This is the main type for your game
    /// </summary>
    public class Game1 : Microsoft.Xna.Framework.Game
    {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;
        Rectangle ball;
        Texture2D text;
        bool leftRight = false;
        bool upDown = false;
        Rectangle pl1;
        Rectangle pl2;
        Rectangle mid;
        int width, height;
        KeyboardState kb;
        Texture2D text2;
        SpriteFont font;
        int pl1Score;
        int pl2Score;

        public Game1()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            graphics.PreferredBackBufferHeight = 600;
            graphics.PreferredBackBufferWidth = 1200;
            graphics.IsFullScreen = false; // desabilita o modo tela cheia 
            graphics.ApplyChanges(); // aplica as mudanças 
            Window.Title = "Pong v.Beta"; // define um título à janela

            base.Initialize();
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);

            width = Window.ClientBounds.Width;
            height = Window.ClientBounds.Height;

            ball = new Rectangle(600, 300, 50, 50);
            pl1 = new Rectangle(20, height/2-50, 20, 100);
            pl2 = new Rectangle(width-40, height/2-50, 20, 100);
            text = Content.Load<Texture2D>(@"SQUARE");
            text2 = Content.Load<Texture2D>(@"CIRCLE");
            font = Content.Load<SpriteFont>("Arial");

            mid = new Rectangle((width / 2 - 1), 0, 1, height);
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// all content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            // Allows the game to exit
            kb = Keyboard.GetState();

            updateKeys();
            updateBallDirection();

            base.Update(gameTime);
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.White);

            spriteBatch.Begin();

            spriteBatch.Draw(text2, ball, Color.White);
            spriteBatch.Draw(text, pl1, Color.Black);
            spriteBatch.Draw(text, pl2, Color.Black);
            spriteBatch.Draw(text, mid, Color.Black);


            spriteBatch.DrawString(font, "Player 1: " + pl1Score, new Vector2(50, 20), Color.Black);
            spriteBatch.DrawString(font, "Player 2: " + pl2Score, new Vector2(650, 20), Color.Black);

            spriteBatch.End();

            base.Draw(gameTime);
        }

        private void updateBallDirection()
        {

            if (ball.Intersects(pl2))
                leftRight = false;

            if (ball.Intersects(pl1))
                leftRight = true;

            if (ball.X < 0)
            {
                pl2Score++;
                leftRight = true;
                resetGame();
            }

            if (ball.X > 1150)
            {
                pl1Score++;
                leftRight = false;
                resetGame();
            }

            if (ball.Y > 550)
                upDown = false;

            if (ball.Y < 0)
                upDown = true;

            if (leftRight == true)
                ball.X += 20;

            if (leftRight == false)
                ball.X -= 20;

            if (upDown == true)
                ball.Y += 20;

            if (upDown == false)
                ball.Y -= 20;
        }

        private void updateKeys()
        {

            if (kb.IsKeyDown(Keys.W) && pl1.Y != 0)
                pl1.Y -= 10;

            if (kb.IsKeyDown(Keys.S) && (pl1.Y + pl1.Height) != height)
                pl1.Y += 10;

            if (kb.IsKeyDown(Keys.Up) && pl2.Y != 0)
                pl2.Y -= 10;

            if (kb.IsKeyDown(Keys.Down) && (pl2.Y + pl2.Height) != height)
                pl2.Y += 10;

            if (kb.IsKeyDown(Keys.Enter))
            {
                pl1Score = 0;
                pl2Score = 0;
                resetGame();
            }

        }

        private void resetGame()
        {
            ball.Y = 300;
            ball.X = 600;
            pl1.X = 20;
            pl1.Y = (height / 2) - 50;
            pl2.X = width - 40;
            pl2.Y = height / 2 - 50;
           
        }
    }
}
